---
title: Baseline Earth - A ProofMode Event for Earth Month 2024
description: Use the ProofMode app to document the world around you
subtitle: Use the ProofMode app to document the world around you
featured_image: /images/thehunt.png
---

Earth Month is a great reminder to celebrate our planet and take action to protect it. This year, we are encouraging people to contribute to [Baseline](/baseline), an environmental and cultural project to document the world with a verifiable and resilient approach. Read on to learn more, and to find out how to enter the "Proof of Earth" Contest.

### Establishing a Baseline

[Proofmode: Baseline](/baseline) is a project to document, authenticate, and safeguard the current state of Earth’s environment, culture, and natural resources using the ProofMode app, tools, and workflows.  The ProofMode app leverages decentralized technology, including IPFS and Filecoin, to capture, notarize, and store verifiable visual evidence. So far, [Baseline has preserved](/preserve) nearly 100GB of photos, videos, digital signatures, and supporting metadata content. with a goal of adding 100TB this year, through an increasing amount of high-resultion photos and 4K+60FPS videos.

<hr/>
*Learn how to resiliently store multimedia content and data on IPFS and Filecoin through the [PRESERVE Process Documentation](/preserve).*
<hr/>

By empowering anyone to contribute to this repository, [Baseline](/baseline) serves as a hub where authentic, verifiable content can persist, ensuring future generations can access authentic and comprehensive records of our world’s natural beauty, cultures, and environmental challenges. 

[![Hurricane Otis 2023](/images/baseline/hurricaneotis2023.png)](https://baseline.proofmode.org/ipfs/QmYJMjG3uBDKVZ11GAJSCahiJ2N6uBseFePiHEwoqQw4GP)
Recently, Baseline was used in the [2023 Hurricane Otis relief efforts](/blog/hurricane-otis-proofmode) to capture and compare photos of Acapulco, Mexico in the wake of the disaster, helping counter misinformation and provide verifiable image and video evidence for relief and recovery efforts.

[![Cape Town,SA 2023](https://baseline.proofmode.org/ipfs/QmPad6dJ4Y9xH62goPCH2KkBkFVXcFKYav6BhV5iWK8N8f/media/large/2023-04-02%20Stellenbosch%20Hot%20Air%20Balloon%20Flight%20(35)/proofmode-7B9EBFF4CBB73FC0-2023-04-02-11-20-30GMT%2B2/IMG_3481.jpg)](https://baseline.proofmode.org/ipfs/QmPad6dJ4Y9xH62goPCH2KkBkFVXcFKYav6BhV5iWK8N8f/)
Last year, musician, photographer, and adventurer [Roland Albertson](https://www.rolandalbertson.com/) documented the [beautiful human and nature settings of Cape Town, South Africa](https://baseline.proofmode.org/ipfs/QmPad6dJ4Y9xH62goPCH2KkBkFVXcFKYav6BhV5iWK8N8f/).

From natural disaster documentation and land rights movements to a year-in-the-life in Cape Town, the role of decentralized technology is becoming crucial in supporting a foundation for action, awareness, and future research for environmental data thanks to its resilient nature. 

### About ProofMode

ProofMode adds extra data to photos and videos, to help people trust that it came from the person, place, and time it claims to be from. ProofMode is a system that enables authentication and verification of multimedia content, particularly captured on a smartphone, from point of capture at the source to viewing by a recipient. It utilizes enhanced sensor-driven metadata, hardware fingerprinting, cryptographic signing, and third-party notaries to enable a pseudonymous, decentralized approach to the need for chain-of-custody and “proof” by both activists and everyday people alike.

You can view a recent [public presentation on ProofMode here](https://www.beautiful.ai/player/-NHPp4pwCgIqJTMhyeBv/ProofMode-The-Hunt-Baseline-and-Beyond)

### Proof of Earth Contest

You can participate in Baseline: Earth, and potentially win an invitation to the [ProofCorps Team](/proofcorps), by doing the following steps:

* Use the [ProofMode app](/install) to document the natural beauty of our planet. 
* Share your photo(s) as a "robust" Proofpack (zip file) to be entered into the contest, using one of the following methods:
	* Use our [PRESERVE](/preserve) process to store proof zip on IPFS and Filecoin, and share the Content ID with us (BEST!)
	* Share via Signal app to +17185697272 (GOOD!)
	* use Dropbox, Google Drive and share link via email to share@proofmode.org (JUST FINE!)

The winner will be invited to join ProofCorps as a paid contributor, a global community of photographers and videographers using ProofMode to document and share cryptographically verified content from around the world.

If you have any questions, feel free to [contact us](/contact)

### More Step-by-Step Info on using ProofMode

You can watch a [video training on using ProofMode](/blog/proofmode-in-the-newsroom) here, and/or follow the steps below:

* Install ProofMode using the instructions here: https://proofmode.org/install
* Test the app out to make sure it is working with your phone and camera
* Open the app and enable Proof
* In Settings, enable all options: Network, Phone, Location, Notary
* Take a picture using the in app camera option
* Take a picture using your phone’s main camera app
* Share “robust” proof of a single photo using the in app options (“Share Proof”)

