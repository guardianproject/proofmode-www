---
title: The Hunt - A ProofMode Interactive Game
description: Use the ProofMode app to play games, win prizes and fight disinformation
subtitle: Use the ProofMode app to play games, win prizes and fight disinformation
featured_image: /images/thehunt.png
---

### Coming Soon: The Hunt NYC! September 22 - 23, 2022

Use the ProofMode app to play The Hunt NYC, a 24-hour disinformation-fighting scavenger hunt where the motto is "cryptographically verified pics or it didn't happen!" Take photos of up to six required items commonly found around NYC in a 24-hour period, share the "robust" proof through Signal to +1 718 569 7272 or email hunt@proofmode.org, and win amazing prizes! 

Learn more on the [THE HUNT NYC Announcement](/blog/proofmode-unfinished-thehuntnyc) page.

![](/images/thehunt-promo-nyc.jpg)

### The Hunt @ DWebCamp 2022 

We brought The Hunt to [DWebCamp 2022](https://dwebcamp.org/) in the redwood forests of Northern California. This location provided a unique offline/offgrid aspect to the game, where all proof was shared through nearby hotspot micro-servers, bluetooth or Air Drop.

![](/images/dweb-proofmode-scavenger-hunt.png)

Below, phones ready to go with ProofMode loaded, sitting above bags of player badge rewards for participation!

![](/images/dweb-buttons.jpg)


