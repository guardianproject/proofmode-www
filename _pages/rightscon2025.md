---
title:  The Hunt Taipei at RightsCon 2025
description: Use ProofMode to play a scavenger hunt, win prizes and defend reality
subtitle: Use ProofMode to play a scavenger hunt, win prizes and defend reality
featured_image: /images/thehunttaipei_header.jpg
layout: minpage
---

### Play the Hunt Taipei!

Join the fun to defend reality against AI fakes, by using the [ProofMode app](/about) to play The Hunt Taipei, a multi-day photo scavenger hunt held during [RightsCon 2025](https://www.rightscon.org/) in Taipei, Taiwan from February 24th to the 27th, 2025. 

![a display card for playing the hunt game](/images/thehunttaipei_header.jpg)

### #1 rule: cryptographically verifiable pics or it didn't happen!

*Fortunately, ProofMode makes that very easy to do! [Install the app](/install) to play*

HOW TO PLAY:
1. Take photos at the famous [Taipei Night Markets](https://guide.michelin.com/en/article/travel/best-night-market-taipei-food-travel-guide) of up to six delicious foods
2. share the proof through [Signal](https://signal.me/#p/+447886176827) or [WhatsApp](https://wa.me/+447886176827) to __+447886176827__ or send/share via email or filesharing to [hunt@proofmode.org](mailto:hunt@proofmode.org)
3. Wait for [ProofMode Verification](/verify) and hope to win amazing prizes! 

*Keep your eyes out for Grilled Squid, Bubble Tea, Soup Dumplings, Beef Noodle Soup, Stinky Tofu and Fried Fish Balls. When you find one, take a photo using ProofMode, and then hopefully enjoy eating it too.*

![a display card for playing the hunt game](/images/thehunttaipei_card.png)

#### Need help or want to connect with other players? > [Join The Hunt Signal Group](https://signal.group/#CjQKIKS1pkywDJbYQoWB_jQam_O_MFgB86DIr8yNcDzY7oOuEhDwCaOBw3KOeqZ8_T_JeQm1) <


Once you are done with the scavenger hunt, use the "Share Proof" feature in ProofMode, to send your set of proof photos to the judges for [ProofCheck Verification](/verify). Depending how fast and complete your proof is delivered, you may win a prize (a super amazing multi-purpose solar panel+battery pack+flashlight). 

<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=ysYrP3-aK6SmlpXC&amp;list=PL4-CVUWabKWdOinhL2O08QvLpor_ZmSjC" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
