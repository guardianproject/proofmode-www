---
title: ProofMode Metadata Detail
description: Learn about the data that is available through Proofmode
subtitle: Learn about the data that is available through Proofmode
---

This page provides details on the fields available in the JSON and CSV files that ProofMode generates and includes within a ProofMode archive (ZIP file).

You can find the "living version" of this document as a [Google Sheet](https://docs.google.com/spreadsheets/d/10_xS2DZ8tX1A0jyxvFwmD8TYANb6Wmkrw89Lvjs01Bk/edit?usp=sharing) or see it in the table below.

We have also published the [ProofMode v1 JSON Schema](https://gitlab.com/guardianproject/proofmode/proofmode-data-tools/-/blob/main/specs/ProofModev1.0.25.schema.json).

<table>
  {% for row in site.data.proofmodedata %}
    {% if forloop.first %}
    <tr style="background:yellow;text-format:bold;">
      {% for pair in row %}
        <th><b>{{ pair[0] }}</b></th>
      {% endfor %}
    </tr>
    {% endif %}
  {% tablerow pair in row %}
      {{ pair[1] }}
    {% endtablerow %}
  {% endfor %}
</table>


