---
title: About ProofMode
description: An open-source project developed by Guardian Project, Okthanks and WITNESS
subtitle: A collaboration between Guardian Project, Okthanks and WITNESS
featured_image: /images/proof-feature.png
---

### We believe in a future, where every camera will have a “Proof Mode” that can be enabled and every viewer an ability to verify-then-trust what they are seeing.

![](/images/proof-feature.png)

ProofMode is a system that enables authentication and verification of multimedia content, particularly captured on a smartphone, from point of capture at the source to viewing by a recipient. It utilizes enhanced sensor-driven metadata, hardware fingerprinting, cryptographic signing, and third-party notaries to enable a pseudonymous, decentralized approach to the need for chain-of-custody and “proof” by both activists and everyday people alike. 

ProofMode supports the [Coalition for Content Provenance and Authentication (C2PA) standard](/c2pa), [Content Credentials](/c2pa) and the [Content Authenticity Initiative](/c2pa).

#### How can I use ProofMode today?

ProofMode is ready to use and publicly available as [production mobile apps](/install), [desktop tools](/project/proofcheck), [developer libraries](/project/proofmode-for-devs) and [verification processes](/verify). We also provide training and support for the use of resilient decentralized storage technology through our [PRESERVE Process](/preserve).

Please see our [projects page](/projects) for the latest details.


**ProofMode is free and open-source software, available on [Gitlab](https://gitlab.com/guardianproject/proofmode) and [Github](https://github.com/guardianproject/proofmode)**

This work has been done as a collaborative effort between  [Guardian Project](https://guardianproject.info), [WITNESS](https://witness.org), and [Okthanks](https://okthanks.com) for nearly a decade. Guardian Project is partially supported by [Rights Action Lab](https://rightsactionlab.org), a nonprofit 501(c)(3) charitable organization.

We have been primarily sponsored by [Witness](https://witness.org) and the [Filecoin Foundation for the Decentralized Web](https://ffdweb.org/).

