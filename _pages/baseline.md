---
title: Baseline
description: Verifiable Visual Database of Our World
subtitle: Verifiable Visual Database of Our World
layout: minpage
---

All images and videos were captured or imported, and verified with ProofMode. Unless otherwise noted, all content is licensed under <a href='http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1'>CC BY-SA 4.0</a>. All content below is preserved and published on [IPFS](https://ipfs.io) and [Filecoin](https://filecoin.io) using a [multi-step, decentralized process](/preserve) that [you can replicate](/preserve) with your own proof content.

### [Hurricane Otis (~3GB) - Acapulco, Mexico - November 2023](#otis2023)

[![Hurricane Otis 2023](/images/baseline/hurricaneotis2023.png)](https://baseline.proofmode.org/ipfs/QmYJMjG3uBDKVZ11GAJSCahiJ2N6uBseFePiHEwoqQw4GP)

A [special report](/blog/hurricane-otis-proofmode) from team member Fabiola, originally from Mexico, who returned to her home region to assist in relief efforts. On October 25th, 2023 the city of Acapulco, Mexico was hit by a Category 5 Hurricane named Otis. Otis was the strongest landfalling Pacific hurricane on record. Early reports and images from Acapulco showed catastrophic damage to structures, including many hotels and high-rise buildings, downed trees, severe flooding, and mudslides. 

[Story Report](/blog/hurricane-otis-proofmode) | [Interactive Gallery](https://baseline.proofmode.org/ipfs/QmYJMjG3uBDKVZ11GAJSCahiJ2N6uBseFePiHEwoqQw4GP) | [HTTPS](https://baseline.proofmode.org/ipfs/QmNNdSt7b9AdWVZiVqUut3QY4QQfcJWoQQQr7CXMPDYVQ7) | [IPFS](ipfs://QmNNdSt7b9AdWVZiVqUut3QY4QQfcJWoQQQr7CXMPDYVQ7) | [IPNS](ipns://k51qzi5uqu5ditxd4npa0nba09efo6zx7u3tsl89q09pwgy3uekhxijrtmxwjg)

### [Cape Town 2023 (~20GB) - South Africa - 2023](#capetown2023)

[![Cape Town,SA 2023](/images/baseline/capetown2023.png)](https://baseline.proofmode.org/ipfs/QmPad6dJ4Y9xH62goPCH2KkBkFVXcFKYav6BhV5iWK8N8f/)

Musician, Photographer, and Adventurer [Roland Albertson](https://www.rolandalbertson.com/) documented the beautiful human and nature settings of Cape Town, South Africa, through the year of 2023.

[Contributor: Roland Albertson](https://www.rolandalbertson.com/) | [Interactive Gallery](https://baseline.proofmode.org/ipfs/QmPad6dJ4Y9xH62goPCH2KkBkFVXcFKYav6BhV5iWK8N8f/) | [HTTPS](https://baseline.proofmode.org/ipfs/QmZNTD4tAe8ELX1VosM8buQQcCg4S9zG5N2Xp2ofqCfSrF) | [IPFS](ipfs://QmZNTD4tAe8ELX1VosM8buQQcCg4S9zG5N2Xp2ofqCfSrF) | [IPNS](ipns://k51qzi5uqu5djlvxpbcdh7ig4xzn26jl08iexm3rzfv1ux9oxhtqa804pomud4)

### [Indigenous Caravan - Mexico - 2023](#caravan2023)

[![caravan 2023](/images/baseline/caravan2023.png)](https://baseline.proofmode.org/ipfs/QmRGKtem8FkzzJ6jVjJUiY3VVUfvrEPo9SFBp89B5yu5nL)

[CARAVANA AND INTERNATIONAL MEETING “EL SUR RESIST 2023.”](https://www.elsurresiste.org/) The caravan left the Chiapas coast on April 25, toured the Isthmus of Tehuantepec from South to North, continued on the Yucatan peninsula, and ended with a meeting between all the organizations that attended the different events of the tour the day May 5 at CIDECI / Caracol Jacinto Canek in San Cristóbal de las Casas, Chiapas.

[Story Report](https://www.elsurresiste.org/) | [Interactive Gallery](https://baseline.proofmode.org/ipfs/QmRGKtem8FkzzJ6jVjJUiY3VVUfvrEPo9SFBp89B5yu5nL) | [HTTPS](https://baseline.proofmode.org/ipfs/QmXcH54WQbVz2sqMjuVD6nFqLxkLMvkvjoFiPMB2vaaDd8) | [IPFS](ipfs://QmXcH54WQbVz2sqMjuVD6nFqLxkLMvkvjoFiPMB2vaaDd8) | [IPNS](ipns://QmXcH54WQbVz2sqMjuVD6nFqLxkLMvkvjoFiPMB2vaaDd8)

{% leaflet_map {"zoom" : 1 } %}
 {% leaflet_marker { "latitude" : -33.9249, "longitude" : 18.4241, "popupContent" : "<a href='https://baseline.proofmode.org/ipfs/QmTKi5YTqB59VAskA9JDMA1dv1LYWgPSbaCNB9UY3fCry7/index.html'>Cape Town 2023</a>"} %}
 {% leaflet_marker { "latitude" : 16.8640, "longitude" : -99.8823, "popupContent" : "<a href='http://proofmode.org/blog/hurricane-otis-proofmode'>Hurricane Otis, Acapulco, Mexico 2023</a>"} %}
{% endleaflet_map %}

### How to Access and Help Preserve

Above are links to the full "Proof Set" of our source eyewitness - original unmodified photos and videos along with [ProofMode verifiable metadata](/verify). There are four methods of accessing them:
* an interactive visual gallery 
* a browseable HTTPS link that provides access to view data and download proof pack zip files
* an [Interplanetary File System (IPFS)](https://dweb-primer.ipfs.io/) "immutable" content identifier link that can be accessed using any IPFS runtime or [IPFS-capable web browser](https://docs.ipfs.tech/install/ipfs-companion/) 
* an [Interplanetary Name System (IPNS)](https://dweb-primer.ipfs.io/) pointer to this proof set that will remain the same, even if we update the proof set with more content

If you would like to help preserve a proof set, you can do so using the following [ipfs pin]([IPFS Primer Guide](https://dweb-primer.ipfs.io/)) command on your local computer or server with [IPFS installed](https://dweb-primer.ipfs.io/):

        > ipfs pin add <content id>

If you would like to download the entire proof set, you can use the following command:

        > ipfs get <content id>

### Join ProofCorps

If you would like to contribute to our Baseline projects, or need support to help document important stories and events, please considering [joining our ProofCorps team](https://proofmode.org/proofcorps). Technical and financial supports is available for those in need.
