---
title: Contact
subtitle: Reach out to us to discuss how you might incorporate ProofMode into your workflow, app, service or device.
description: Reach out to us to discuss how you might incorporate ProofMode into your workflow, app, service or device.
featured_image: /images/proof-feature02.png
---

You can talk with us live on our [ProofMode Public Matrix Room](https://matrix.to/#/#proofmode:matrix.org) or [via Convene](https://letsconvene.im/app/#/room/#proofmode:matrix.org)

To find out all the ways you can connect to us, visit the [Guardian Project Contact Page](https://guardianproject.info/contact).

You can also [email our help desk team](mailto:support@guardianproject.info) or [chat with us securely on Matrix](https://matrix.to/#/#guardianproject:matrix.org)


