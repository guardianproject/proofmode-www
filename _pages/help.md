---
title: Get Help
description: Videos, guides and more to help with using ProofMode
subtitle: Videos, guides and more to help with using ProofMode
layout: minpage
---

### Help Video Playlist

<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=3F1S2a5jebfggdma&amp;list=PL4-CVUWabKWdOinhL2O08QvLpor_ZmSjC" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

<a href="https://youtube.com/playlist?list=PL4-CVUWabKWdOinhL2O08QvLpor_ZmSjC&feature=shared">View the full playlist on YouTube</a>

### Presentations

* Splintered Networks, Unbroken Proof [Video and Slides](/blog/splintered-networks)
* ProofMode for Journalists and in the Newsroom  [Slides](https://www.beautiful.ai/player/-NfDEunb5lNGfa3hoiTC) and [Video](https://www.youtube.com/watch?v=SXaP_8veUyk)
* [ProofMode+C2PA: Interoperable Ecosystems](https://www.beautiful.ai/player/-NfSKZFXm2jVKtr9hy3o)

### Contact Us

* <a href="https://guardianproject.info/contact/">Get in touch with the Guardian Project team</a>
* Get help through [Signal](https://signal.me/#p/+447886176827) or [WhatsApp](https://wa.me/+447886176827) to __+447886176827__ or send via email to [support@guardianproject.info](mailto:support@guardianproject.info)



