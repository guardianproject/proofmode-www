---
title: All the Links
description: Specs, Talks, Support and More
subtitle: Everything we have to share in one place
layout: minpage
---

### Projects

* [ProofMode for iOS](/project/proofmode-ios)
* [ProofMode for Android](/project/proofmode-android)
* [libProofMode Developer Library](/project/proofmode-for-devs)
* [ProofCheck Verification Tool](/project/proofcheck)
* [SimpleC2PA Mobile SDK](https://gitlab.com/guardianproject/proofmode/simple-c2pa)
* [ProofMode BASELINE: Verifiable Visual Database of Our World](/baseline/)
* [ProofMode on GitLab: All of our free and open-source code](https://gitlab.com/guardianproject/proofmode)
* [ProofMode PRESERVE: How to resiliently store media on decentralized networks](/preserve/)

### Specifications, Papers, and Guides

* [ProofMode Metadata Documentation v1](/metadata)
* [Three Layer Verification: Integrity, Consistency, Synchrony](https://proofmode.org/blog/three-layers)
* [What is ProofMode? Brochure](/docs/ProofmodeBrochure.pdf)
* [ProofMode and Content Credentials (C2PA)](/c2pa)

### Presentations

* Splintered Networks, Unbroken Proof [Video and Slides](/blog/splintered-networks)
* ProofMode for Journalists and in the Newsroom  [Slides](https://www.beautiful.ai/player/-NfDEunb5lNGfa3hoiTC) and [Video](https://www.youtube.com/watch?v=SXaP_8veUyk)
* [ProofMode+C2PA: Interoperable Ecosystems](https://www.beautiful.ai/player/-NfSKZFXm2jVKtr9hy3o)
* [Data Feminism: An Intersectional Approach to Gathering, Analyzing, and Sharing](https://docs.google.com/presentation/d/1k0XFnJ4NYUeoL9kPz5DRKi6OMMSnPCaEV1E-rQb0D1s/edit?usp=sharing)

### Videos

* Livestream session hosted by the [Content Authenticity Initiative](https://contentauthenticity.org) with Adobe and others working on the C2PA standard [YouTube Stream](https://www.youtube.com/live/Pep48gA_X6Y?feature=shared&t=2280)
* FLOSS Weekly - Show Me The ProofMode [YouTube Stream](https://www.youtube.com/watch?v=8YHgF81Ot_U)
* Witness.org Tech+Advocacy ProofMode [YouTube Stream](https://www.youtube.com/watch?v=l7tMack-vZM&pp=ygUJcHJvb2Ztb2Rl)

### Articles

* [Authenticity in 3-D: Verifying Photogrammetric Models](https://commonplace.knowledgefutures.org/pub/6lwe0y5m/release/3#n949ajdignv)
* [The Economist "Proving a photo is fake is one thing. Proving it isn’t is another"](https://www.economist.com/science-and-technology/2023/01/09/proving-a-photo-is-fake-is-one-thing-proving-it-isnt-is-another)
* [Government Internet Shutdowns Are Changing. How Should Citizens and Democracies Respond?](https://carnegieendowment.org/2022/03/31/government-internet-shutdowns-are-changing.-how-should-citizens-and-democracies-respond-pub-86687)
* [Tracking metadata can be useful -- and proper](https://www.computerworld.com/article/2488304/tracking-metadata-can-be-useful----and-proper.html)
