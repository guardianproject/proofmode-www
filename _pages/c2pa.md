---
title: ProofMode and C2PA
description: Our Support for Provenance and Authentication
subtitle: Our Support for Provenance and Authentication
---

We are excited to announce support for the [Coalition for Content Provenance and Authentication (C2PA)](https://c2pa.org/) standard, [Content Credentials](https://contentcredentials.org/) and the [Content Authenticity Initiative](https://contentauthenticity.org/).

![proofmode and c2pa diagram](/images/proofmodeandc2paslide.png)

Our [ProofMode mobile apps](/projects) and [ProofCheck Verification Tool](/project/proofcheck) now incorporate the [open-source C2PA toolkits](https://gitlab.com/guardianproject/proofmode/simple-c2pa) to enable the creation and inclusion of claims, attestations, and manifests, to ensure an open, interoperable chain of provenance and authentication in the digital media they capture and import.

![proofmode and c2pa diagram](/images/proofmodeandc2pa.png)

You can try this out today in our latest release of [ProofMode for Android and iOS](/install). You can also see links below to our Simple C2PA Developer Library for our easy to use streamlined solution for implementing C2PA support in your own app.

### Links

* Simple C2PA Developer Library [Main Project](https://gitlab.com/guardianproject/proofmode/simple-c2pa) - [Android](https://gitlab.com/guardianproject/proofmode/simple-c2pa-android) - [iOS](https://gitlab.com/guardianproject/proofmode/simple-c2pa-ios)
* [ProofMode+C2PA: Interoperable Ecosystems](https://www.beautiful.ai/player/-NfSKZFXm2jVKtr9hy3o)
* Livestream session hosted by the [Content Authenticity Initiative](https://contentauthenticity.org) with Adobe and others working on the C2PA standard [YouTube Stream](https://www.youtube.com/live/Pep48gA_X6Y?feature=shared&t=2280)

### CAI Community Showcase

<iframe width="340" height="240" src="https://www.youtube.com/embed/Pep48gA_X6Y?si=ZUN-dKSLxF9LIUU6&amp;start=2282" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

