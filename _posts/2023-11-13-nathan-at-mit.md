---
title: 'Nathan at the MIT Decentralized Web Salon'
date: 2023-11-13 00:00:00
featured_image: '/images/posts/nathanmit1123.png'
excerpt: Nathan presents the latest work on ProofMode at a special event at MIT
---

The [Filecoin Foundation for the Decentralized Web](https://ffdweb.org) and [MIT Open Learning](https://openlearning.mit.edu/) hosted an Open Learning Salon on October 26 at MIT's campus in Cambridge.  The focus of this event was to explore advancements in decentralized technologies and their transformative impact, as well as new opportunities for applying decentralized technologies.

ProofMode project lead Nathan Freitas, presented on "ProofMode: Verifiable Decentralized Knowledge - Capturing and Preserving Essential Observations of Reality". As part of this, he was asked to consider four key questions, which are covered in the slides, video, and in text added to the bottom of the post.

[Full presentation slides](https://www.beautiful.ai/player/-Nj9655PQDAF1YF1pHTv)

[<img src="/images/posts/nathanmit1123.png"/>](https://www.youtube.com/watch?v=jg5oqYQttS4&t=602s)
[YouTube video from MIT Open Learning](https://www.youtube.com/watch?v=jg5oqYQttS4&t=602s)

**Question: What could a decentralized web look like?**

* Apps and services that can work with and without internet connectivity, that can share and storage data nearby, that can operated on user data without it being stored on a centralized cloud service

**Question: How can decentralized tech affect social change?**

* Communities can document their own causes, news and culture, and retain control of their media and data, while ensuring it can be distributed, preserved and trusted

**Question: Who are the key players?**

* Open-source projects, frontline documentarians, marginalized communities, truth tellers, community networks, academic institutions, lawyers, libraries and archives

**Question: What’s at stake and&nbsp;what is education’s role?**

* Trust in reality. (simple!)... Education must teach, enable, disseminate, pioneer, and model.




