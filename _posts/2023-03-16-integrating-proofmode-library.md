---
title: 'Insights from Ngenge Senior, ProofMode Integration Developer'
date: 2023-03-16 10:00:00
featured_image: '/images/integrating-proofmode.jpg'
excerpt: ProofMode’s purpose has been to enable any individual to generate proof of images and videos they take using their phones. One of our main goals has been to enable other third-party camera app developers to have this power of proof generation in their own apps without necessarily having to get all of ProofMode’s code.
---

*Ngenge Senior is a member of the ProofMode development team, working on integration of libProofMode into third-party applications. Here is an excerpt and link to a recent blog post on the subject.*

ProofMode has arguably been one of the most exciting projects I have worked on as an Android developer.

ProofMode’s purpose has been to enable any individual to generate proof of images and videos they take using their phones. This proof can be shared with an entity such as a lawyer in court when need be. Though there has been much advancement in the Android and iOS apps, one of our main goals has been to enable other third-party camera app developers to have this power of proof generation in their own apps without necessarily having to get all of ProofMode’s code.

Read the [full post here](https://ngengesenior.medium.com/integrating-proofmode-in-simple-camera-android-315b79394020)

[![image of sample code](/images/posts/libproofmode_integration.png)](https://ngengesenior.medium.com/integrating-proofmode-in-simple-camera-android-315b79394020)


