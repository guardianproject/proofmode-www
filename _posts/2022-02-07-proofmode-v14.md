---
title: 'ProofMode v14 (0.0.14-RC3) released!'
date: 2022-02-07 00:00:00
featured_image: '/images/posts/proofmode-on-the-blockchain.jpg'
excerpt: A new release with improved proof reliability and easier verification
---

A new release of ProofMode for Android is now available, [tagged on Github as 0.0.14 RC-3 aka v14](https://github.com/guardianproject/proofmode/releases/tag/0.0.14-RC-3), for [direct download on our website](https://guardianproject.info/releases/ProofMode-0.0.14-RC-3-artwork-release.apk), and soon on [Google Play](https://play.google.com/store/apps/details?id=org.witness.proofmode) and [F-Droid](https://guardianproject.info/fdroid).

- major improvements to proof generation reliability
- fix for mediafile HASH generation to wait until Android finishes processing media files (fix for hash not matching previously)
- new proofmode ZIP format sharing with public key and Opentimestamps OTC file included for easier verification
- better integration with Signal sharing and other apps
- new translations to new languages!

On the topic of [Opentimestamps.org](https://opentimestamps.org) verification, we have made it easier for non-technical people to perform a verification against the notarization of the mediafile submitted by the Proofmode app. In the new proofmode ZIP file, there will be an .OTS (Open Time Stamp) for each media file. It will be named with the sha256sum hash of the media file it matches to. You just need to [Opentimestamps.org](https://opentimestamps.org) to upload the OTS file and the matching media file, and the site will verify they match, and display the history and status of the notarization process on the Bitcoin blockchain.  

![easy verification with opentimestamps](/images/posts/proofmode-on-the-blockchain.jpg)
(A successful verification!)

Within every Proof ZIP file, we also now include a simple ["HowToVerifyProofData.txt"](https://github.com/guardianproject/proofmode/blob/master/app/src/main/assets/HowToVerifyProofData.txt) instructions document. This lays out, how using open tooling like sha256sum, GPG or any OpenPGP tooling, you can verify the integrity of the media file and proof CSV data, and connect it with third party services to further verify notarization and identity.

In the next few weeks, we'll be working on improved data formats for the proof data, improving the CSV format, while also including an easier to process JSON format. This aligns well with our new [Lightning Bug "Consistency Check"](/blog/lightning-bug) work underway by Jack Fox Keen. 

Find out how to join us in this work through our [Contact Page](/contact), and of course you can submit bug reports [through email](mailto:support@guardianproject.info) or [Github](https://github.com/guardianproject/proofmode). 
