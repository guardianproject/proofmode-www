---
title: 'Sam Gregory of WITNESS discusses ProofMode'
date: 2021-12-09 00:00:00
featured_image: '/images/posts/samwitness.jpg'
excerpt: In this era of “fake news” and “alternative facts” ProofMode makes it possible for anyone to produce trustworthy media
---

Together with the Guardian Project WITNESS developed and launched ProofMode, a mobile authentication tool designed to ensure that citizen stories can be trusted. The app runs in the background of mobile phones and with one click, users can add extra digital proof in the form of corroborating metadata to every photo/video. In this era of “fake news” and “alternative facts” ProofMode makes it possible for anyone to produce trustworthy media in the moment and makes it harder to tamper with images after the fact.

<iframe width="560" height="315" src="https://www.youtube.com/embed/l7tMack-vZM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



