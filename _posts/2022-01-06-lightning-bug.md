---
title: 'A Remarkable Thing Happens'
date: 2022-01-06 00:00:00
featured_image: '/images/posts/blog_image_1_010622.png'
excerpt: A remarkable thing happens during a protest--separate entities come together and work collectively for the common good, finding rhythm in marches, chants, singing, and more. 
---

*A research note by Jack Fox Keen, Data Empowerment Lead on ProofMode*

ProofMode is an app which strives to find the balance between veracity and privacy, by collecting informed and consensual metadata about an image to prove its authenticity while respecting and protecting the user's privacy. ProofMode's utility in social justice movements provides a unique window into the world of activists and humanitarians that can shed light on complex phenomena in our chaotic world.

A remarkable thing happens during a protest--separate entities come together and work collectively for the common good, finding rhythm in marches, chants, singing, and more. Through this lens, protests can be viewed as a chaotic system which yields synchronicity, akin to lightning bugs flashing luminescence in unison after a short period of time together.

![an image of lightning bug light](/images/posts/blog_image_1_010622.png)

[Learning to love nature through the lightning bug](https://www.humansandnature.org/learning-to-love-nature-through-the-lightning-bug)

Across miles of river banks, lightning bugs flash in unison as if coordinated by a maestro. This phenomenon puzzled biologists for centuries, and was attributed to tricks of the eye as a mirage due to the pattern-seeking behavior of our brains. However, it was observed that taking jars of lightning bugs and introducing them into a room could demonstrate the synchronization in real time. First two fireflies would synchronize, then three, and multiple pockets would begin to pulse in unison until as many as a dozen flies were in sync.

How would we see this unfold in a protest? Through social contagion, a domino effect of photography and videography spiraling outward from the apex of activity. As one person captures the moment, another one does, and another, until there is an orchestra of documentation happening in unison. Potentially, these spirals of contagion have uniquely identifying patterns, a distinct fingerprint of the event itself. We could potentially see distinct differences between the social contagion effect at a wedding, versus how it unfurls in a protest. The incipience of a revolution will, hypothetically, leave a different data imprint than a tourist attraction. Classifications of these digital fingerprints may help journalists detect when a cataclysmic event is unfolding, alerting news agencies to respond faster to rapidly escalating situations.

![a graph of social contagion](/images/posts/blog_image_2_010622.gif)

By utilizing both “big” and “little” data analytics, we hope to empower those on the frontlines with the tools necessary to document human rights violations. Our goal is to legitimize citizen journalism as well as assist conventional news organizations to archive pivotal events as they are happening in real time. ProofMode will allow activists to prove the integrity of their archived footage and give journalists a faster reaction time to critical moments.



