---
title: 'The League of Extraordinary Data Legends'
date: 2023-04-06 00:00:00
featured_image: /images/data_legend_histogram_total.svg
excerpt: An explanation of our data legend and ProofCheck data analysis
---

*This post is part of a continuing series providing insight and detail on how ProofMode works, specifically our [“3 Layer Verification”](https://proofmode.org/blog/three-layers) check.*

Our consistency check builds off of our [Metadata Detail](https://proofmode.org/metadata) page on the Proofmode website. Again, we refer to our living [Data Legend](https://docs.google.com/spreadsheets/d/10_xS2DZ8tX1A0jyxvFwmD8TYANb6Wmkrw89Lvjs01Bk/edit#gid=0) document, which has been updated to include how our data is read by our Consistency Check. We have also published the [ProofMode v2 JSON Schema](https://gitlab.com/guardianproject/proofmode/proofmode-data-tools/-/blob/schema-updates/specs/schemas/ProofMode.v2.schema.json).

In our Consistency Check on [ProofCheck](https://proofcheck-dev.gpfs.link), we use several different logic schemas, depending on the metadata attribute being analyzed. The histograms reflect the *outliers*, that is, the attributes which do *not* satisfy the logic schemas. *These are flags to signal further inspection, not errors to indicate falsity*:

1. **1:1 Correspondence Logic (Histogram)**: This is for attributes where there should be one unique attribute for each photo. For example, if there are 5 photos per device, there should be 5 distinct file hashes per device.
    * File Hash SHA256, File Modified, File Path, Proof Generated, SafetyCheckTimestamp

![](/images/data_legend_histogram_safety_check_timestamp.svg)

2. **1 Unique Occurrence per Device Logic (Histogram)**: This is for attributes where the attribute should be consistent for all photos within the same device. For example, there should be 1 hardware attribute for all photos in a given device.
    * DeviceID, Hardware, Manufacturer, ScreenSize, Wifi MAC, Network, Network Type, Data Type, Cell Info, IPv4, IPv6, Location Provider, SafetyCheckBasicIntegrity, SafetyCheckCtsMatch,  Language, Locale
    * For WifiMAC, Network, Network Type, Data Type, Cell Info, and Location Provider, it is not unreasonable for there to be some variation in a given batch of photos per device, but when there is variation, we want to call attention to it for the user to examine and verify.

![](/images/data_legend_histogram_location_provider.svg)

3. **Minimum of 1 Occurrence per Device Logic (Histogram)**: This is for attributes where we expect there to be at least one occurrence per device. For example, we would expect at least one location latitude occurrence per device (assuming this setting is turned on!).
    * Location Latitude, Location Longitude, Location Accuracy, Location Bearing, Location Speed, Location Altitude, Location Time

![](/images/data_legend_histogram_location_lat.svg)

4. **0 Occurrences per Device Logic (Histogram)**: This is for attributes where we expect there to be zero occurrences.
    * Notes
    * Our “Notes” field will provide information about whether the media file was captured with our in-app camera, and which version of ProofMode was used. We flag this to indicate to the user that there is some pertinent information to read in this field.

![](/images/data_legend_histogram_notes.svg)

5. **Checking for the Existence of Null Values Logic (Barchart)**: For all of the following attributes, we have a check which goes through each *photo* and determines whether or not there are any null values for these attributes.
    * DeviceID, Hardware, Manufacturer, ScreenSize, Wifi MAC, Network, Network Type, Data Type, Cell Info, IPv4, IPv6, Location Provider, SafetyCheckBasicIntegrity, SafetyCheckCtsMatch,  Language, Locale, Location Latitude, Location Longitude, Location Accuracy, Location Bearing, Location Speed, Location Altitude, Location Time
    * This is separate from the above histograms which is a summary of flags per *device*. This set of logic generates a simple barchart summarizing flags per *photo*, ordered from the least number of flags to the greatest number of flags.

![](/images/data_legend_barchart.svg)
