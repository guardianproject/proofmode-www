---
title: 'ProofMode Provides Verifiable Camera App for 2024 US Election'
date: 2024-10-29 10:00:00
excerpt: Press Release - Mobile app allows users to capture tamper-resistant photos and videos for election documentation, empowering organizations and citizens to document the 2024 US Election with assurance
author: Nathan
categories: [election]
tags: election, newsroom, journalism
tag: election, newsroom, journalism
featured_image: /art/proof-feature.png
---

*[Download Press Release PDF here](/docs/ProofMode2024USElection-PressRelease-October292024.pdf)*

Boston, MA - ProofMode today announced the availability of its free verifiable camera app for Android and iOS and web-based ProofCheck verification tools, empowering organizations and citizens to document the 2024 US Election with assurance. The app allows users to capture photos and videos that are cryptographically signed and timestamped, creating a tamper-resistant record that can be used as “Self-Authenticating Evidence” (Federal Rules of Evidence 902) in court. To do this, ProofMode implements the Coalition for Content Provenance and Authenticity (C2PA), an open technical standard providing publishers, creators, and consumers the ability to trace the origin of different types of media.

"Voter confidence in the electoral process is paramount for a healthy democracy," said Nathan Freitas, Director at Guardian Project, maker of ProofMode. "Our app gives people a simple tool to create and share verifiable visual records of their election experiences, whether that's checking in at a polling place, observing vote counting, or documenting any irregularities." 

![/art/proof-feature.png](/art/proof-feature.png)

Key features of the ProofMode app include:

* Cryptographic signing and timestamping of all captured media
* Tamper-evident audit trails to ensure photos/videos haven't been edited
* Easy sharing of verified media in uncompressed format to news outlets and election officials
* Web-based ProofCheck tools for metadata review and verification 

ProofMode was recently used in Latin America by a major election monitoring organization who found it to be the “fastest way to take a trusted photo”, and played an important role in documenting post-hurricane recovery efforts from Hurricane Otis in 2023.

The ProofMode app is available for free on the Apple App Store and Google Play via [proofmode.org/install](https://proofmode.org/install). You can watch a “ProofMode for Journalists and in the Newsroom” training, and learn how to verify received proof at: [proofmode.org/verify](https://proofmode.org/verify).


About ProofMode
ProofMode is an open-source project developed in collaboration by Guardian Project, Okthanks and WITNESS for over 10 years. It has received funding from the Knights News Foundation, the Filecoin Foundation for the Decentralized Web, the Starling Lab for Data Integrity and other generous supporters.

For more information, visit the [ProofMode website](https://proofmode.org) or [email info@proofmode.org](mailto:info@proofmode.org) with any questions.





