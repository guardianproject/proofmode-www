---
title: 'BASELINE: Hurricane Otis Relief Efforts in Acapulco, Mexico'
date: 2023-11-16 00:00:00
featured_image: '/images/posts/hurricaneotis.jpg'
categories: [baseline, mexico, otis]
tags: baseline
excerpt: A ProofMode Baseline report from our direct work in support of relief and recovery efforts
---

___A special report from team member Fabiola, originally from Mexico, who returned to her home region to assist in relief efforts___

On October 25th, 2023 the city of Acapulco, Mexico was hit by a [Category 5 Hurricane named Otis](https://www.nesdis.noaa.gov/news/hurricane-otis-causes-catastrophic-damage-acapulco-mexico). Otis was the [strongest landfalling Pacific hurricane on record](https://en.wikipedia.org/wiki/Hurricane_Otis). Early reports and images from Acapulco showed catastrophic damage to structures, including many hotels and high-rise buildings, downed trees, severe flooding, and mudslides. Damage was also reported at 120 hospitals and clinics. Additionally, more than 10,000 utility poles were destroyed, knocking out power and communications across the region. Numerous transmission lines, electrical substations, and a power plant were also heavily damaged.

![Hurricane Otis satellite image](/images/posts/hurricaneotis.jpg)

Through our own personal connections, and with support from the [Filecoin Foundation for the Decentralized Web](https://ffweb.org/) for our [ProofMode project](/about), we were able to visit the disaster zone one week after the hurricane to provide relief support and document the damage caused to local infrastructure. At the moment of our visit, this area was still without power and running water. 

<center>
<video width="360" height="240" controls><source src="https://baseline.proofmode.org/ipfs/QmYJMjG3uBDKVZ11GAJSCahiJ2N6uBseFePiHEwoqQw4GP/media/large/Hurricane%20Otis.mp4" type="video/mp4"></video>
</center>
___Before and After visual evidence documented with ProofMode___

### How to Access and Help Preserve

Below are links to the full "Proof Set" of our source eyewitness evidence of this trip - original unmodified photos and videos along with [ProofMode verifiable metadata](/verify). There are four methods of accessing them:
* an interactive visual gallery 
* a browseable HTTPS link that provides access to view data and download proof pack zip files
* an [Interplanetary File System (IPFS)](https://dweb-primer.ipfs.io/) "immutable" content identifier link that can be accessed using any IPFS runtime or [IPFS-capable web browser](https://docs.ipfs.tech/install/ipfs-companion/) 
* an [Interplanetary Name System (IPNS)](https://dweb-primer.ipfs.io/) pointer to this proof set that will remain the same, even if we update the proof set with more content

Hurricane Otis - ProofMode Baseline<br/>Proof Set | [Interactive Gallery](https://baseline.proofmode.org/ipfs/QmYJMjG3uBDKVZ11GAJSCahiJ2N6uBseFePiHEwoqQw4GP) | [HTTPS Browse](https://baseline.proofmode.org/ipfs/QmNNdSt7b9AdWVZiVqUut3QY4QQfcJWoQQQr7CXMPDYVQ7) | [Direct via IPFS](ipfs://QmNNdSt7b9AdWVZiVqUut3QY4QQfcJWoQQQr7CXMPDYVQ7) | [Direct via IPNS](ipns://k51qzi5uqu5ditxd4npa0nba09efo6zx7u3tsl89q09pwgy3uekhxijrtmxwjg)

If you would like to help preserve this proofpack, you can do so using the following [ipfs pin]([IPFS Primer Guide](https://dweb-primer.ipfs.io/)) command on your local computer or server with [IPFS installed](https://dweb-primer.ipfs.io/):

	> ipfs pin add QmNNdSt7b9AdWVZiVqUut3QY4QQfcJWoQQQr7CXMPDYVQ7 

If you would like to download the entire proof set (~3GB), you can use the following command:

	> ipfs get QmNNdSt7b9AdWVZiVqUut3QY4QQfcJWoQQQr7CXMPDYVQ7 


### More Story and Proof From the Disaster Zone

Using a rental truck and escorted by the security detail of local contacts, we traveled to the area to bring supplies, mainly drinking water and canned food. We were also able to document the state of the streets and houses using our [ProofMode camera app](/about). To be able to safely access the areas with the help of local officials,  we were asked to wear uniforms from the organization we represented. We  were also happy to share the credit of any aid we were providing with the officials that helped us access the most isolated and damaged areas of the city.

Below is a photo of ProofCorps team member Fabiola delivering supplies:
[![team delivering supplies](https://baseline.proofmode.org/ipfs/QmYJMjG3uBDKVZ11GAJSCahiJ2N6uBseFePiHEwoqQw4GP/media/large/Delivering%20Supplies%20Acapulco/Screen%20Shot%202023-11-09%20at%209.57.46%20AM.png)](https://baseline.proofmode.org/ipfs/QmYJMjG3uBDKVZ11GAJSCahiJ2N6uBseFePiHEwoqQw4GP/Delivering-Supplies-Acapulco-Delivering-supplies-Acapulco.html)
___Verify this image yourself using our [ProofCheck Verification Tool](https://proofcheck.gpfs.link/#QmNNdSt7b9AdWVZiVqUut3QY4QQfcJWoQQQr7CXMPDYVQ7/Delivering%20Supplies%20Acapulco/Delivering%20supplies%20Acapulco.zip)___

The streets away from the main roads and tourist area were covered with debris, mud, fallen trees, trash, and all the furniture and appliances damaged by the flood that people took out of their houses. It was hard to access even with a truck and there was a foul smell in the air.  When we talked to the locals we learned that many were already showing symptoms of gastrointestinal, skin and eye infections because of the accumulated waste and the lack of running water to disinfect food and clean personal items.
[![Proof Pack Photo gallery](/images/posts/hurricaneotisgallery.jpg)](https://baseline.proofmode.org/ipfs/QmYJMjG3uBDKVZ11GAJSCahiJ2N6uBseFePiHEwoqQw4GP)
___Gallery viewer for Hurricane Otis Proof Set___

We also learned that the people with gas generators were charging $30 Mexican Pesos ( about $1.5 USD ) to charge cell phones and only two battery lines. This is a lot of money for the people living in that part of the city and could be used to buy medicine or supplies. Most of them opted to stay offline and without access to information.

It was surprising to see that even in that desperate situation people lined up and waited patiently to receive their supplies, and because we only had 10 first aid kits they all agreed that injured people or people with sick relatives should get them. It was also surprising to see the community leaders listen carefully to the instructions on how to connect and use technology we had provided, while their phones were charging on the solar power bank we brought with us.

People were tired, thirsty, and hungry, but at least we managed to connect devices to charge from the power bank we provided. People in that area had been without electricity for a week, their goal in going to the relief center to charge their phones was to try to communicate with family and friends in the area or outside the city.

### Documenting with ProofMode 

The city devastated by Hurricane Otis is a very famous tourist attraction known for its beautiful beaches, fancy hotels, and nightlife. It has been estimated that the hurricane damaged 80% of the hotel infrastructure in the area. It was also revealed that only 60% of the affected businesses were insured, and the other 40% will rely on government support to restore their business. In both cases business owners are seeing this catastrophe as an opportunity to build an improved version of the port city that will be more attractive for national and international visitors.

<center>
<video width="480" height="720" controls><source src="https://baseline.proofmode.org/ipfs/QmNNdSt7b9AdWVZiVqUut3QY4QQfcJWoQQQr7CXMPDYVQ7/Nissan%20Costera/Nissan%20Costera/1699139255147.mp4" type="video/mp4"></video>
</center>
[Verify this video of the Nissan Costera car dealership](https://proofcheck.gpfs.link/#bafybeiaaqg565xjltxsohjwnawfwrlys6qeioiaiw4wrln2ypixanqidqq/Nissan%20Costera/Nissan%20Costera.zip)

In order to achieve that insured businesses are rushing to make their insurance claims, and as part of a deal with the government. Insurance companies are providing an advance on the claims once validated with the proper documentation and evidence to remove all the debrief from the affected properties. 

{% leaflet_map {"zoom" : 5 } %}
    {% leaflet_geojson "/images/posts/otis/dcf7356f-5214-4408-9552-7f4310beb406-geojson.json" %}
{% endleaflet_map %}
___Verified map data from ["Hotspot for power and wifi" proof pack](https://baseline.proofmode.org/ipfs/QmYJMjG3uBDKVZ11GAJSCahiJ2N6uBseFePiHEwoqQw4GP/Hotspot-for-power-and-wifi-proofmode-96E603C84CFBC993-2023-11-10-19-30-16GMT-6.html) available as [GeoJSON Data](https://baseline.proofmode.org/ipfs/QmNNdSt7b9AdWVZiVqUut3QY4QQfcJWoQQQr7CXMPDYVQ7/Hotspot%20for%20power%20and%20wifi/dcf7356f-5214-4408-9552-7f4310beb406-geojson.json)___

While we were on the ground we were able to visit some of the most luxurious and famous hotels and document some of the visible damage to their infrastructures. Most of the furniture and appliances, plus all the decoration and gardens were totally lost and needed to be removed as a first step. Documenting the damage to buildings using ProofMode for insurance claims provides additional digital fingerprints and cryptographic signatures for verification of the evidence gathered.

We also used ProofMode to track the use of the satellite internet mobile station we sponsored, to ensure it is being used in areas with less access to service and communication. It has been a great accountability tool to build trust with our partners on the ground.

[![Starlink setup](/images/posts/otisstarlink.jpg)](https://baseline.proofmode.org/ipfs/QmYJMjG3uBDKVZ11GAJSCahiJ2N6uBseFePiHEwoqQw4GP/Starlink.html)
[Verify Starlink deployment image](https://proofcheck.gpfs.link/#QmNNdSt7b9AdWVZiVqUut3QY4QQfcJWoQQQr7CXMPDYVQ7/Starlink/Starlink%20Setup%203.zip)

### About ProofMode Baseline

Our work to verify, preserve and now publish this media, has been done to provide public documentation of the Hurricane Otis aftermath and relief efforts. Our goal is to counter any mis or disinformation regarding the event, and provide verifiable image and video evidence that can be used to inform additional relief, recovery, and documentarian activities. The entire ["proof pack" set](https://baseline.proofmode.org/ipfs/QmNNdSt7b9AdWVZiVqUut3QY4QQfcJWoQQQr7CXMPDYVQ7/) is published on our [ProofMode Baseline](/baseline/) infrastructure, which is powered by the [Interplantery File System](https://ipfs.io) and [Filecoin](https://filecoin.io/) decentralized storage networks. This means that once published to a specific IPFS Content Identifier (CID), as shown earlier in this post, the contents stored are [immutable and cannot change](https://docs.ipfs.tech/concepts/immutability/). 

If you would like to contribute to our [Baseline project](/baseline/), or need support to help document important stories and events, please considering [joining our ProofCorps team](https://proofmode.org/proofcorps). Technical and financial supports is available for those in need.

If you would like to help preserve this proofpack, you can do so using the following "ipfs pin" command:

	> ipfs pin add QmNNdSt7b9AdWVZiVqUut3QY4QQfcJWoQQQr7CXMPDYVQ7 

Learn more about how to use IPFS on your local machine or server, through this [IPFS Primer Guide](https://dweb-primer.ipfs.io/)
