---
title: 'Of Moods and Means'
date: 2022-07-20 10:00:00
featured_image: '/images/posts/of-moods.jpg'
excerpt: Activism exists in the irrealis mood–things that we wish to be true, that could be true, that might be true, but are not necessarily true. 
---

__by Jack Fox Keen__

![](/images/posts/of-moods.jpg)

Image by [Jasmine Co](https://www.jasminecoart.com/) (with permission and compensation)

Activism exists in the [irrealis mood](https://en.wikipedia.org/wiki/Irrealis_mood)–things that we wish to be true, that could be true, that might be true, but are not necessarily true. This includes the subjunctive, the conditional, and the hypothetical.  As activists, we live at the conjunction of reality and “irreality”–between intangible dreams and concrete building blocks. We want things that have never been done before. I feel this profoundly as a prison abolitionist: How can one even begin to imagine a world without prisons? Where does one begin to implement this? We would have to [change everything](https://www.haymarketbooks.org/books/1597-change-everything), as aptly quipped by Ruth Wilson Gilmore.

Yet we have to do things now to reach our dreams. That’s the “active” part of being an activist. The “dreaming” is living in irrealis; the “building” is the process of taking real, actionable steps right now as activists. We have to organize, petition, read, write, communicate, and ultimately shift public consciousness. From the infrastructures of decentralized  food sovereignty networks (such as [Food Not Bombs](https://foodnotbombs.net/new_site/)) to the decentralized web archive storage of proof of warcrimes (such as [Starling sLab’s use](https://hackmd.io/buOMlBRbToiHVZhPvUhexQ) of [IPFS](https://ipfs.io/)), we need actionable, achievable steps to transform the irrealis into a reality.

Ironically, despite activists living in the world of dreams, we use language to actualize, manifest, realize our goals. Our slogans are in the  indicative (stating facts and opinions) and imperative (giving orders and instructions) moods. They are demands, not requests. Our dreams have firm roots in reality. They stem from the lived experiences of oppressed people. They are direct responses, opposing reactions, to the harms that take place. The [subjunctive](https://www.merriam-webster.com/words-at-play/getting-in-the-subjunctive-mood#:~:text=The%20subjunctive%20mood%20expresses%20wishes,is%20in%20the%20subjunctive%20mood.) mood (for expressing wishes, proposals, suggestions, or imagined situations) grows from the indicative. Our dreams grow from our experiences.

One of our thoughts has been to do an anthropological study–with informed consent of volunteers–of the language and mood which we use in different contexts. How would one measure the irrealis moods? Specifically the subjunctive mood, where we have wishes, hypotheticals, and requests. When does the suggestive subjunctive mood move into the imperative order–when does imagining a world without militarized police and increased human resources for social needs turn into the slogan “Defund the Police”? Interestingly, a [previous study](https://aclanthology.org/W15-0115.pdf), using customer reviews, has shown that it is easier to detect wishes over suggestions.

Arguably, the most difficult piece would be finding a corpus of the subjunctive mood. Such a corpus, according to the aforementioned study, may not even exist. However, I would argue that such a corpus potentially exists in the realm of philosophy and political texts, especially in the world of activists. Would it be possible to use [Project Gutenberg](https://www.gutenberg.org/), for example, to scrape a corpus of political texts from the French Revolution, or to use publicly available [communiques](https://radiozapatista.org/?page_id=3365&lang=en) from the Zapatistas, to create a corpus of the subjunctive text? Perhaps it would be even easier to start with Spanish, where verb conjugations exist for the subjunctive mood.

Using these texts as our baseline, we can begin to understand the tension between our seemingly intangible dreams and our physical building blocks for (and sometimes roadblocks to) those dreams. With an optional “journal entry” feature in ProofMode (which would be an opt-in feature, on top of the user-autonomy to share these journal entries) we can begin to explore the subjunctive mood, the world of dreams and ideas, across cultures, space, and time.




