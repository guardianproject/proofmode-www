---
title: 'Splintered Networks, Unbroken Proof'
date: 2025-01-29 10:00:00
featured_image: '/images/posts/proofmodeoffline.png'
excerpt: Nathan shares work on using ProofMode during Internet shutdowns and outtages.
author: Nathan
categories: [newsroom]
tags: offline, shutdowns, newsroom, journalism
tag: offline, shutdowns, newsroom, journalism
---

Nathan shares work for capturing and preserving verifiable eyewitness reporting during internet outtages and shutdowns, sometimes called "Splinternets".
This solution builds on our Butter Box (https://likebutter.app) and ProofMode projects. ButterBox is a private, local micro-server hotspot, that allows for the secure upload and storage of multimedia evidence, along with other features.

We can also provide online and in-person trainings for humanitarian organizations, news organization, journalists, election monitoring groups, and others. [Contact us here](/contact)

<iframe width="560" height="315" src="https://www.youtube.com/embed/0xwEABNP_JE?si=yVcgkE5_4jFc1yBb" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

The full slides from this presentation [are available here](https://www.beautiful.ai/player/-OHnU1qiwGqblFlHzKjG). 

More presentations, videos, and documentation available [on our Links page](/links)
