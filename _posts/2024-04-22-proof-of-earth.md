---
title: 'Proof of Earth Contest'
date: 2024-04-22 10:00:00
featured_image: '/images/posts/earth.jpg'
excerpt: Earth Month is a great reminder to celebrate our planet and take action to protect it.
author: Nathan
categories: [baseline]
tags: baseline,earth,contest
tag: baseline,earth,contest
---

### Earth Month is a great reminder to celebrate our planet and take action to protect it.

<center>
<video width="720" height="480" autoplay loop muted poster="/images/posts/earth.jpg">
  <source src="/images/hotairsm.mp4" type="video/mp4" />
  Your browser does not support the video tag.
</video>
</center>

This year, we are encouraging people to contribute to [Baseline](/baseline), an environmental and cultural project to document the world with a verifiable and resilient approach. Visit the [Proof of Earth page](/earth) to learn more, and to find out how to enter the "Proof of Earth" Contest.


[![Cape Town,SA 2023](https://baseline.proofmode.org/ipfs/QmPad6dJ4Y9xH62goPCH2KkBkFVXcFKYav6BhV5iWK8N8f/media/large/2023-04-02%20Stellenbosch%20Hot%20Air%20Balloon%20Fli
ght%20(35)/proofmode-7B9EBFF4CBB73FC0-2023-04-02-11-20-30GMT%2B2/IMG_3481.jpg)](https://baseline.proofmode.org/ipfs/QmPad6dJ4Y9xH62goPCH2KkBkFVXcFKYav6BhV5iWK8N8f/)

*Video and photo by Roland Albertson - see more on [Baseline](/baseline#capetown2023)*
