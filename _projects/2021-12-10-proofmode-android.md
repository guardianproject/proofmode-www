---
title: 'ProofMode for Android'
subtitle: 'Free and open-source Android app'
date: 2021-12-10 00:00:00
featured_image: '/images/playstore.png'
excerpt: Learn where you can find our Android app and how to contribute to its development.
---

ProofMode for Android is now available on <a href="https://play.google.com/store/apps/details?id=org.witness.proofmode">Google Play</a>, <a href="https://guardianproject.info/fdroid">F-Droid</a> and <a href="https://github.com/guardianproject/proofmode/releases">Github for direct download</a>.

* [Gitlab open-source project](https://gitlab.com/guardianproject/proofmode/proofmode-android) ([Github Mirror](https://github.com/guardianproject/proofmode-android))

<hr/>
[<img src="/images/proofmodeplaystore.jpg">](https://play.google.com/store/apps/details?id=org.witness.proofmode)
