---
title: 'ProofCheck Verification Tool'
subtitle: 'Web-based utility for verification of proof zip bundles'
date: 2020-03-23 00:00:00
featured_image: '/images/icanproveit.png'
excerpt: Learn how to verify proof
---

* ProofCheck public web tool [https://check.proofmode.org](https://check.proofmode.org)
* Gitlab open-source projects: [ProofCheck Web Interface](https://gitlab.com/guardianproject/proofmode/proofcheck-web), [ProofCheck Node library](https://gitlab.com/guardianproject/proofmode/proofcheck-node), [ProofCheck Data Tools](https://gitlab.com/guardianproject/proofmode/proofmode-data-tools)

<hr/>
<a href="https://check.proofmode.org"><img src="/images/proofchecktaipei.jpg"/></a>
